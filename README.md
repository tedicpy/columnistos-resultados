# Cómo se utilizan estos scripts

## Descarga y primer corrida

**1.** Clonar el repositorio en tu carpeta personal

*Pre:* se necesita git instalado (y un manejo básico del mismo)

```
cd ~ && git clone https://gitlab.com/tedicpy/columnistos-resultados.git
```

**2.** Ir a tu carpeta personal y abrir columnistos-resultados.Rproj con rstudio

*Pre:* tener instalado:
* R (r-base y r-recommended)
* Rstudio

```
cd ~ && rstudio columnistos-resultados.Rproj
```

**3.** Ir al menú Abrir o al ícono correspondiente y abrir los scripts .R y ejecutar paso a paso

**Nota:** Verificar sobre todo que la instalación inicial y carga de librerías se haga correctamente. Si da error, busque en Internet por los mensajes que muestra y solucione las dependencias.

## Cómo contribuir

Podés forkear el código y luego hacernos un PR
