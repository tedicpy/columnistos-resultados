# Instalo paquetes
#install.packages("RSQLite")
#install.packages("tidyverse")
#install.packages("lubridate")
#install.packages("svglite")


library(RSQLite)
library(tidyverse) # Incluye dplyr, ggplot, tidyr, tibble
library(lubridate)
library(svglite)
# NOTA: ignore los conflictos de dplyr

# Armo las URLs de descarga
base_nombre="py_columnistos.sqlite"
base_registro="py_log.txt"
base_url_nombre=paste0("http://columnistos.tedic.net/",base_nombre)
base_url_registro=paste0("http://columnistos.tedic.net/",base_registro)

# Descargo el archivo y lo sustituyo cada vez
download.file(base_url_nombre,base_nombre)
download.file(base_url_registro,base_registro)

# Extraigo la fecha del archivo descargado
base_fecha <- scan(base_registro)

# Abro la conexión con la BBDD descargada
conn <- dbConnect(dbDriver("SQLite"),base_nombre)

# Esto no es necesario pero se hace a modo didáctico
# Guardo las tablas de la base
tablas <- dbListTables(conn)
# Listo campos de todas las tablas
for (indice in 1:length(tablas)){
  print(tablas[indice])
  print(dbListFields(conn,tablas[indice]))
}

## Ahora sí ¡a lo nuestro!
# Creo consulta y la guardo como dataframe
base_orig <- dbFetch(dbSendQuery(conn, "select * from articles a join authors aut where a.author_id = aut.id;"))
# Desconecto
dbDisconnect(conn)
# Nota: va dar un WARNING, ignorar

# Guardo la base en formato R simplemente para archivo - descomente de ser necesario
#nombre_arch <- paste0(base_fecha,'_columnistos.RData')
#save(base_orig, file=nombre_arch)

# Para que quede más linda (hay que leer que son estas tibble)
base_orig <- as_tibble(base_orig)
# La muesro
base_orig

# Borro los archivos descargados
file.remove (c(base_nombre, base_registro))

###################################
# 1. Aprendiendo la magia de dplyr

###################################
# 1.1 Aquí usando "pipes" %>%
# - filtro solo las mujeres
# - muestro solo 4 columnas
base_orig %>%
  filter(gender == "F") %>%
  select(title, added, site, author)

# OBS: la base queda inmutada y no se guarda el resultado en ninguna variable
# OBS2: fecha se ve en un formato largo y con hora

###################################
# 1.2 Uso select, mutate y filter
# - ahora guardo el resultado en otra base
arts <- base_orig %>%
  # Me quedo con las var q quiero y las renombro
  select(id,titulo=title,url,autor=author,added,sitio=site,genero=gender) %>%
  # Cambio fecha a mejor formato
  mutate(fecha = as.Date(added)) %>%
  # Creo una columna mes, que saque de la fecha y ponga con etiqueta
  mutate(mes = month(fecha, label=TRUE)) %>%
  mutate(anio = year(fecha)) %>%
  # Borrro variable vieja
  select(-added) %>%
  # Saco casos indefinidos
  filter(genero != 'X') 

# La muestro
arts
# OBS: ahora la fecha se ve linda

###################################
# 1.3 Hay duplicados??

# Veo sus dimensiones
#  y las variables que quedaron
dim(arts)
names(arts)

# Creo una base nueva pero solo con la URL
arts2 <- arts %>%
  # Me quedo solo con la url
  select(url) %>%
  # Y ahora solo con las filas no duplicadas
  distinct()
dim(arts2)

# Resto la cantidad de filas de ambas
dim(arts)[1] - dim(arts2)[1]

# Esto significa que (tomando la URL como identificador) hay 23 artículos repetidos
# FIXME: después hemos de ver cómo lo solucionamos

###################################
# 1.3 Ahora filtro sin guardar
# - ahora hay nombres nuevos en las variables
# - muestro solo los artículos de 2019
arts %>%
  filter(fecha <= '2019-12-31',fecha >= '2019-01-01')

# - solo con los de 2018
arts %>%
  filter(fecha <= '2018-12-31',fecha >= '2018-01-01')

# filtro febrero de 2019 y ahora guardo
arts_2019feb <- arts %>%
  filter(fecha >= '2019-02-01',fecha <= '2019-02-28')

# idem solo 2019 y guardo
arts_2019 <- arts %>%
  filter(fecha <= '2019-12-31',fecha >= '2019-01-01')

###################################
# 1.4 Ahora genero tabla
# - genero x medio
genero_sitio <- arts %>%
  group_by(sitio,genero) %>%
  summarise (total = n())
# y la muestro
genero_sitio
# Guardo en archivo
write.csv(genero_sitio, file="graficos/genero_sitio.csv")

# - genero x fecha
genero_fecha <- arts %>%
  group_by(fecha,genero) %>%
  summarise (total = n())
# y la muestro
genero_fecha
# Guardo en archivo
write.csv(genero_fecha, file="graficos/genero_fecha.csv")

# - genero x mes 2018
genero_mes_2018 <- arts %>%
  group_by(mes,genero) %>%
  filter(fecha<='2018-12-31',fecha >= '2018-01-01') %>%
  summarise (total = n())

# - genero x mes 2019
genero_mes_2019 <- arts %>%
  group_by(mes,genero) %>%
  filter(fecha<='2019-12-31',fecha >= '2019-01-01') %>%
  summarise (total = n())
genero_mes_2019
# Guardo tabla en archivo
write.csv(genero_mes_2019, file="graficos/genero_mes_2019.csv")

# - genero x anio
genero_anio <- arts %>%
  group_by(anio,genero) %>%
  summarise (total = n())
genero_anio
# Guardo tabla en archivo
write.csv(genero_anio, file="graficos/genero_anio.csv")

# Artículos por sitio y por fecha
# - con esto descubirmos que LaNación dejó de registrar el 29feb
sitio_mes_2019 <- arts %>%
  group_by(fecha,sitio) %>%
  filter(fecha<='2019-12-31',fecha >= '2019-01-01') %>%
  summarise (total = n())
sitio_mes_2019
# Guardo tabla en archivo
write.csv(sitio_mes_2019, file="graficos/sitio_mes_2019.csv")

###################################
# 2. Graficamos con ggplot

# Defino una variable que usaré para los gáficos
base_chafe <- paste0(day(ymd(base_fecha)),"/",month(ymd(base_fecha)),"/",year(ymd(base_fecha)))
pie <- paste0("(Datos generados a partir de base disponible en http://columnistos.tedic.net - datos hasta ",base_chafe,")")
###################################
# 2.1 puntos: articulos por fecha 
# - tomo la tabla anterior
# - parto en dos los datos, por un lado F y por otro M
ggplot(data=genero_fecha) + 
  geom_point(mapping = aes(x=fecha, y=total)) + 
  facet_wrap(~ genero)

###################################
# 2.2 barras: artículos vs genero
# - tomo toda la base
# - le agrego color
ggplot(data=arts_2019) + 
  geom_bar(mapping = aes(x=genero, fill=genero)) + 
  # Título, subtítulo, etc
  labs(title = "Columnistsos", 
     subtitle = "Columnas por género 2019",
     caption = pie)
ggsave("graficos/genero_2019.jpg")
ggsave("graficos/genero_2019.svg")

###################################
# 2.3 columnas: artículos vs genero
# - tomo solo 2019
# - columnas 
ggplot(data=genero_mes_2019) + 
  geom_col(mapping = aes(x=mes, y=total, fill=genero),position= "dodge") +
  # Título, subtítulo, etc
  labs(title = "Columnistsos", 
     subtitle = "Columnas escritas por hombres y mujeres en 2019",
     caption = pie)
ggsave("graficos/genero_mes_2019.jpg")
ggsave("graficos/genero_mes_2019.svg")

# - ahora vamos a nombrar los ejes
ggplot(data=genero_mes_2019) + 
  geom_col(mapping = aes(x=mes, y=total, fill=genero),position= "dodge") + 
  # Etiquetas en los ejes
  scale_y_continuous("Total de artículos") + 
  scale_x_discrete("Meses") + 
  # Título, subtítulo, etc
  labs(title = "Columnistsos", 
     subtitle = "Columnas escritas por hombres y mujeres en 2018",
     caption = pie)
# Guardo el último gráfico
# - ojo que sustituye al anterior si ya lo tenía
# - lo pongo en el ignore de git para que no se suba
ggsave("graficos/genero_mes_2019.jpg")
ggsave("graficos/genero_mes_2019.svg")

# - ahora los años
# - le aplico la funcion factor a año para que no lo represente como número (con decimales)
ggplot(data=genero_anio) + 
  geom_col(mapping = aes(x=factor(anio), y=total, fill=genero),position= "dodge") + 
  # Etiquetas en los ejes
  scale_y_continuous("Total de artículos") + 
  scale_x_discrete("Años") + 
  # Título, subtítulo, etc
  labs(title = "Columnistsos", 
       subtitle = "Columnas escritas por género y año",
       caption = pie) + 
  # Aplico pequeños cambios estéticos
  theme_light()

# Guardo el último gráfico
# - ojo que sustituye al anterior si ya lo tenía
ggsave("graficos/genero_anio.jpg")
ggsave("graficos/genero_anio.svg")

# género por sitio
ggplot(data=genero_sitio) + 
  geom_col(mapping = aes(x=sitio, y=total, fill=genero),position= "dodge") + 
  # Etiquetas en los ejes
  scale_y_continuous("Total de artículos") + 
  scale_x_discrete("Medios de difusión") + 
  # Título, subtítulo, etc
  labs(title = "Columnistsos", 
       subtitle = "Columnas escritas en cada medio 2018-2019",
       caption = pie)
ggsave("graficos/genero_sitio.jpg")
ggsave("graficos/genero_sitio.svg")
